package demo2Server;

import java.io.IOException;
import server.HttpProtocolUtil;
import server.HttpServlet;
import server.Request;
import server.Response;

public class demo2Servlet extends HttpServlet {
    @Override
    public void doGet(Request request, Response response) {

        String content = "<h1>LagouServlet Demo2 get</h1>";
        try {
            response.output((HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(Request request, Response response) {
        String content = "<h1>LagouServlet Demo2 post</h1>";
        try {
            response.output((HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws Exception {

    }

    @Override
    public void destory() throws Exception {

    }
}
